#!/bin/bash

payorUser=weblogic
payorHost=172.29.28.34
payorClaimServerHosts=172.29.28.35,172.29.28.36,172.29.28.37,172.29.28.38,172.29.28.39,172.29.28.32,172.29.28.43,172.29.28.44
weblogicDomainDir=/home/weblogic/oracle/wls_12.2.1.2.0/user_projects/domains/prod
payorEnvironmentDir=/home/weblogic/environments/prod

payorWLXmlFolder=${weblogicDomainDir}/xml
customFilesFolder=${payorEnvironmentDir}/custom-files/xml


echo "Copying MVAConfig.xml from custom-files.txt to the admin Weblogic Server from bcbsaz_post_install_script."
ssh -q  ${payorUser}@${payorHost} "cp ${customFilesFolder}/MVAConfig.xml ${payorWLXmlFolder}"

export IFS=","
for payorClaimServerHost in $payorClaimServerHosts; do
echo "Copying MVAConfig.xml from admin WL server to claim server ${payorClaimServerHost} from bcbsaz_post_install_script."
ssh -q ${payorUser}@${payorHost} "scp ${payorWLXmlFolder}/MVAConfig.xml ${payorUser}@${payorClaimServerHost}:${payorWLXmlFolder}"

done

exit 0
